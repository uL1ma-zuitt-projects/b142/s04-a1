package com.obrera.b142.s02.s02app.controllers;

import com.obrera.b142.s02.s02app.models.Post;
import com.obrera.b142.s02.s02app.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class PostController {
    @Autowired
    PostService postService;

    @RequestMapping(value="/users/{userId}/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestBody Post newPost, @PathVariable Long userId) {
        postService.createPost(newPost, userId);
        return new ResponseEntity<>("New post was created.", HttpStatus.CREATED);
    }

    @RequestMapping(value="/posts", method=RequestMethod.GET)
    public ResponseEntity<Object> getPosts() {
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

    @RequestMapping(value="/users/{userId}/posts", method=RequestMethod.GET)
    public ResponseEntity<Object> getMyPosts(@PathVariable Long userId) {
        return new ResponseEntity<>(postService.getMyPosts(userId), HttpStatus.OK);
    }

    @RequestMapping(value="/users/{userId}/posts/{postId}", method=RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable Long userId, @PathVariable Long postId, @RequestBody Post updatedPost){
        postService.updatePost(userId, postId, updatedPost);
        return new ResponseEntity<>("Post was updated.", HttpStatus.OK);
    }

    @RequestMapping(value="/posts/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long id){
        postService.deletePost(id);
        return new ResponseEntity<>("Post was deleted", HttpStatus.OK);
    }
}
